package enjeen;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class EnjeenError extends RuntimeException {
  private static final long serialVersionUID = 353743225999171625L;


  public EnjeenError(Throwable cause) {
    super(cause);
  }


  public EnjeenError(String message) {
    super(message);
  }


  public EnjeenError(String message, Throwable cause) {
    super(message, cause);
  }


  public static void checkGL() {
    int error = GL11.glGetError();
    if (error != GL11.GL_NO_ERROR) {
      throw new EnjeenError(String.format("OpenGL error %d: %s", error, GLU.gluErrorString(error)));
    }
  }
}
