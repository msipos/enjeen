package enjeen.gui;

import enjeen.gl.Color;
import enjeen.gl.Prim;
import enjeen.gl.TrueTypeFont;

/** Elementary theme engine. */
public class ThemeEngine {
  private final TrueTypeFont font;
  private final Color textColor, bgColor, borderColor;
  private final int borderWidth;


  public ThemeEngine(TrueTypeFont font, Color textColor, Color bgColor, Color borderColor, int borderwidth) {
    this.font = font;
    this.textColor = textColor;
    this.bgColor = bgColor;
    this.borderColor = borderColor;
    this.borderWidth = borderwidth;
  }


  /** Render an unbordered box. */
  public void renderBox(final int x1, final int y1, final int x2, final int y2) {
    this.bgColor.set();
    Prim.box4i(x1, y1, x2, y2);
    Color.reset();
  }


  /** Render a bordered box. */
  public void renderBorderedBox(final int x1, final int y1, final int x2, final int y2) {
    this.borderColor.set();
    Prim.box4i(x1, y1, x2, y2);
    this.bgColor.set();
    Prim.box4i(x1+borderWidth, y1+borderWidth, x2-borderWidth, y2-borderWidth);
    Color.reset();
  }

}
