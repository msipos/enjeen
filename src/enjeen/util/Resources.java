package enjeen.util;

import java.io.InputStream;

public class Resources {
  private static final ClassLoader cl = Resources.class.getClassLoader();


  public static InputStream getStream(String resource) {
    return cl.getResourceAsStream(resource);
  }
}
