package enjeen.util;

public class LwjglDlls {
  public static void unpack() {
    DllUnpacker unpacker = new DllUnpacker();
    // Linux
    unpacker.addLinuxDll(32, "rsc/native/linux/libjinput-linux32.so");
    unpacker.addLinuxDll(32, "rsc/native/linux/liblwjgl32.so");
    unpacker.addLinuxDll(32, "rsc/native/linux/libopenal32.so");
    unpacker.addLinuxDll(64, "rsc/native/linux/libjinput-linux64.so");
    unpacker.addLinuxDll(64, "rsc/native/linux/liblwjgl64.so");
    unpacker.addLinuxDll(64, "rsc/native/linux/libopenal64.so");
    // Windows
    String path = "rsc/native/windows";
    unpacker.addWindowsDll(32, path + "jinput-dx8.dll");
    unpacker.addWindowsDll(32, path + "jinput-raw.dll");
    unpacker.addWindowsDll(32, path + "lwjgl.dll");
    unpacker.addWindowsDll(32, path + "OpenAL32.dll");
    unpacker.addWindowsDll(64, path + "jinput-dx8_64.dll");
    unpacker.addWindowsDll(64, path + "jinput-raw_64.dll");
    unpacker.addWindowsDll(64, path + "lwjgl64.dll");
    unpacker.addWindowsDll(64, path + "OpenAL64.dll");
    // Mac OSX
    path = "rsc/native/macosx";
    unpacker.addMacDll(32, path + "libjinput-osx.jnilib");
    unpacker.addMacDll(32, path + "liblwjgl.jnilib");
    unpacker.addMacDll(32, path + "openal.dylib");
    unpacker.addMacDll(64, path + "libjinput-osx.jnilib");
    unpacker.addMacDll(64, path + "liblwjgl.jnilib");
    unpacker.addMacDll(64, path + "openal.dylib");
    unpacker.unpackAndSet();
  }
}
