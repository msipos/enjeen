package enjeen.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;


public class DllUnpacker {
  private static final int LINUX = 1, WINDOWS = 2, MACOS = 3;


  private static int detectOS() {
    final String os = System.getProperty("os.name").toLowerCase();
    if (os.contains("win")) return WINDOWS;
    if (os.contains("nux")) return LINUX;
    if (os.contains("mac")) return MACOS;
    return 0;
  }


  private static int detectBitness() {
    final String os = System.getProperty("os.arch");
    if (os.contains("64")) return 64;
    return 32;
  }


  /** How this works:
   * Explanation: At first the system property is updated with the new value. This might be a relative path – or maybe
   * you want to create that path dynamically.  The Classloader has a static field (sys_paths) that contains the paths.
   * If that field is set to null, it is initialized automatically. Therefore forcing that field to null will result
   * into the reevaluation of the library path as soon as loadLibrary() is called.
   *
   * From http://blog.cedarsoft.com/2010/11/setting-java-library-path-programmatically/. */
  private static void setDllPath(String path) {
    System.setProperty("java.library.path", path);

    try {
      Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
      fieldSysPath.setAccessible(true);
      fieldSysPath.set(null, null);
    } catch (IllegalArgumentException ex) {
      System.err.println(ex);
    } catch (IllegalAccessException ex) {
      System.err.println(ex);
    } catch (NoSuchFieldException ex) {
      System.err.println(ex);
    } catch (SecurityException ex) {
      System.err.println(ex);
    }
  }


  private final ArrayList<Dll> dlls = new ArrayList<Dll>();


  public void addLinuxDll(int bitness, String path) {
    dlls.add(new Dll(LINUX, bitness, path));
  }
  public void addWindowsDll(int bitness, String path) {
    dlls.add(new Dll(WINDOWS, bitness, path));
  }
  public void addMacDll(int bitness, String path) {
    dlls.add(new Dll(MACOS, bitness, path));
  }


  private void unpack(String classpath, File destFile) {
    InputStream is = null;
    try {
      is = Resources.getStream(classpath);
      if (is == null) {
        System.err.printf("no such resource: '%s'%n", classpath);
        return;
      }

      FileOutputStream fos = null;
      try {
        fos = new FileOutputStream(destFile);

        final byte[] array = new byte[1024];
        int len;
        while ((len = is.read(array)) != -1) {
          fos.write(array, 0, len);
        }
      } catch (IOException ex) {
        System.err.printf("error while unpacking '%s' to '%s'%n", classpath, destFile.getAbsolutePath());
        /* Ignore. */
      } finally {
        try {
          if (fos != null) fos.close();
        } catch (IOException ex) { /* Ignore. */ }
      }
    } finally {
      try {
        if (is != null) is.close();
      } catch (IOException ex) { /* Ignore. */ }
    }
  }


  public void unpackAndSet() {
    final File tmpdir = new File(System.getProperty("java.io.tmpdir"));
    final File dlldir = new File(tmpdir, "JavaDllUnpacker");
    dlldir.mkdirs();

    if (!dlldir.isDirectory()){
      System.err.println("Could not create directory '" + dlldir.getAbsolutePath() + "'");
      return;
    }

    final int os = detectOS();
    final int bitness = detectBitness();

    for (Dll dll : dlls) {
      if (os == dll.getOS() && bitness == dll.getBits()) {
        final String classpath = dll.getPath();
        int index = classpath.lastIndexOf('/');
        if (index == -1) index = 0;
        String filename = classpath.substring(index);

        final File destFile = new File(dlldir, filename);
        unpack(classpath, destFile);
      }
    }

    setDllPath(dlldir.getAbsolutePath());
  }
}
class Dll {
  private final int OS, bits;
  public int getOS() { return OS; }
  public int getBits() { return bits; }


  private final String path;
  public String getPath() { return path; }


  public Dll(int OS, int bits, String path) {
    this.OS = OS;
    this.bits = bits;
    this.path = path;
  }
}