package enjeen.util;

import enjeen.gl.TrueTypeFont;
import java.util.Random;

public class GameUtil {
  /** A utility random number generator */
  public static Random rng;

  /** Main font */
  public static TrueTypeFont mainFont = null;
  public static TrueTypeFont getMainFont() { return mainFont; }

  public static void setup() {
    rng = new Random();
  }
}
