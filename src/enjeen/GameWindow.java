package enjeen;

import enjeen.gl.view.View;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class GameWindow {
  private final int width, height;
  public int getHeight() { return height; }
  public int getWidth() { return width; }


  private final View view;
  public View getView() { return view; }


  public GameWindow(int width, int height) {
    this.width = width;
    this.height = height;
    view = View.create2D(width, height);

    try {
      Display.setDisplayMode(new DisplayMode(width, height));
      Display.setVSyncEnabled(true);
      Display.create();
    } catch (LWJGLException ex) {
      throw new EnjeenError(ex);
    }
  }


  public void destroy() {
    Display.destroy();
  }
}
