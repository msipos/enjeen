package enjeen;

import enjeen.event.Draw2DEvent;
import enjeen.event.MainLoop;
import enjeen.gl.*;
import enjeen.gl.view.View;
import enjeen.util.LwjglDlls;
import java.io.File;

public class Test {
  public static void main(String[] args) {
    LwjglDlls.unpack();
 //   if (args[0].equals("test1")) {
      test2();
 //   }
  }


  public static void test1() {
    GameWindow window = new GameWindow(800, 600);
    final View view = View.create2D(100, 100, 200, 200);
    final View view2 = View.create2D(500, 100, 200, 200);
//    MainLoop loop = new MainLoop() {
//      private float phi = 0.0f;
//
//
//      @Override
//      public void eventGLDisplay2D() {
//        view.set();
//        GL11.glBegin(GL11.GL_TRIANGLES);
//        float A = 100.0f;
//        GL11.glVertex2f(100.0f+(float) (A*Math.sin(phi)), 100.0f+(float) (A*Math.cos(phi)));
//        GL11.glVertex2f(100.0f+(float) (A*Math.sin(2*phi)), 100.0f+(float) (A*Math.cos(2*phi)));
//        GL11.glVertex2f(100.0f+(float) (A*Math.sin(3*phi)), 100.0f+(float) (A*Math.cos(3*phi)));
//        GL11.glEnd();
//
//        view2.set();
//        GL11.glBegin(GL11.GL_TRIANGLES);
//        GL11.glVertex2f(100.0f+(float) (A*Math.sin(phi)), 100.0f+(float) (A*Math.cos(phi)));
//        GL11.glVertex2f(100.0f+(float) (A*Math.sin(2*phi)), 100.0f+(float) (A*Math.cos(2*phi)));
//        GL11.glVertex2f(100.0f+(float) (A*Math.sin(3*phi)), 100.0f+(float) (A*Math.cos(3*phi)));
//        GL11.glEnd();
//      }
//
//
//      @Override
//      public void eventUpdate(int delta) {
//        phi += 0.001f * delta;
//      }
//    };
//    loop.loop();
    window.destroy();
  }


  public static void test2() {
    GameWindow window = new GameWindow(800, 600);
    Gl.enableAlphaBlend();
    MainLoop loop = new MainLoop(window);
    final TrueTypeFont ttf = TrueTypeFont.newFromFile(new File("test/font/font.ttf"), 16.0f);
    Draw2DEvent event = new Draw2DEvent() {
      @Override
      public void draw2d() {
        final Color color = new Color(0.2f, 0.2f, 0.2f);
        color.set();
        Texture.unbind();
        Prim.box4i(5, 5, 15 + ttf.getTexture().getPixelWidth(), 15 + ttf.getTexture().getPixelHeight());
        Prim.image2i(10, 10, ttf.getTexture());

        ttf.drawString(20.0f, 550.0f, "Hello world", 1.0f, 1.0f, TrueTypeFont.ALIGN_LEFT);

      }
    };
    loop.addHandler(event);
    loop.loop();
    window.destroy();
  }
}
