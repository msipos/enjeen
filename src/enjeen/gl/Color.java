package enjeen.gl;

import org.lwjgl.opengl.GL11;

public class Color {
  public final float red, green, blue, alpha;
  
  public Color(float red, float green, float blue) {
    this.red = red; this.green = green; this.blue = blue; this.alpha = 1.0f;
  }
  
  public Color(float red, float green, float blue, float alpha) {
    this.red = red; this.green = green; this.blue = blue; this.alpha = alpha;    
  }
  
  public void set() {
    GL11.glColor4f(red, green, blue, alpha);
  }
  
  public static void reset() {
    GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  }
}
