package enjeen.gl;

import org.lwjgl.opengl.GL11;

public class Atlas {
  private final Texture tex;
  private final float dx;
  private final float dy;

  public Texture getTex() {
    return tex;
  }

  public Atlas(Texture tex, int dx, int dy) {
    this.tex = tex;
    this.dx = ((float) dx) / ((float) tex.getPixelWidth());
    this.dy = ((float) dy) / ((float) tex.getPixelHeight());
    //Log.info("dx=%f, dy=%f", this.dx, this.dy);
  }

  public void vertex(int x, int y) {
    GL11.glTexCoord2f(x*dx, y*dy);
  }
}
