package enjeen.gl;

import enjeen.EnjeenError;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.opengl.GL12;
import org.lwjgl.util.glu.GLU;

public class Texture {
  private final String textureName;
  private final int textureId;


  private final int pixelWidth, pixelHeight;
  public int getPixelHeight() { return pixelHeight; }
  public int getPixelWidth() { return pixelWidth; }


  private Texture(final BufferedImage image, final String textureName) {
    this.textureName = textureName;
    this.pixelWidth = image.getWidth();
    this.pixelHeight = image.getHeight();
    glEnable(GL_TEXTURE_2D); EnjeenError.checkGL();
    textureId = glGenTextures(); EnjeenError.checkGL();

    bind();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); EnjeenError.checkGL();

    // Load pixel data into a buffer
    final ByteBuffer buffer = ByteBuffer.allocateDirect(pixelWidth * pixelHeight * 4);

    // Load each mipmap
    loadBuffer(buffer, image, 1);
    GLU.gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, pixelWidth, pixelHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    EnjeenError.checkGL();

    makeBlocky();
    makeClamped();
  }


  public static Texture newFromImage(final BufferedImage image) {
    return new Texture(image, "loaded from image");
  }


  public static Texture newFromFile(final File file) {
    BufferedImage image = null;
    try {
      image = ImageIO.read(file);
    } catch (IOException ex) {
      throw new EnjeenError(String.format("could not load image '%s': %s",
                                          file.getAbsolutePath(),
                                          ex.getLocalizedMessage()),
                            ex);
    }
    return new Texture(image, file.getName());
  }


  public void makeBlocky() {
    bind();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); EnjeenError.checkGL();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR); EnjeenError.checkGL();
  }


  public void makeLinear() {
    bind();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); EnjeenError.checkGL();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); EnjeenError.checkGL();
  }


  public void makeClamped() {
    bind();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE); EnjeenError.checkGL();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE); EnjeenError.checkGL();
  }


  public void makeModulate() {
    bind();
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  }

  private void loadBuffer(final ByteBuffer buffer, final BufferedImage image, final int factor) {
    buffer.position(0);
    for (int x = 0; x < pixelWidth; x+= factor) {
      for (int y = 0; y < pixelHeight; y+= factor) {
        int rgba = image.getRGB(x, y);
        byte alpha = (byte) ((rgba >> 24) & 0xFF);
        byte red = (byte) ((rgba >> 16) & 0xFF);
        byte green = (byte) ((rgba >> 8) & 0xFF);
        byte blue = (byte) (rgba & 0xFF);
        int pos = y * pixelWidth * 4 + x * 4;
        buffer.put(pos, red);
        buffer.put(pos + 1, green);
        buffer.put(pos + 2, blue);
        buffer.put(pos + 3, alpha);
      }
    }
    buffer.position(0);
  }


  public void bind() {
    glBindTexture(GL_TEXTURE_2D, textureId); EnjeenError.checkGL();
  }


  public static void unbind() {
    glBindTexture(GL_TEXTURE_2D, 0); EnjeenError.checkGL();
  }


  public void destroy() {
    glBindTexture(GL_TEXTURE_2D, 0); EnjeenError.checkGL();
    glDeleteTextures(textureId); EnjeenError.checkGL();
  }
}
