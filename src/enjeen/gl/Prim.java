package enjeen.gl;

import static org.lwjgl.opengl.GL11.*;

/** Some elementary primitives. */
public class Prim {
  public static void box4i(int x1, int y1, int x2, int y2) {
    glBegin(GL_QUADS);
    glVertex2i(x1, y1);
    glVertex2i(x1, y2);
    glVertex2i(x2, y2);
    glVertex2i(x2, y1);
    glEnd();
  }


  public static void image2i(int x, int y, Texture tex) {
    final int w = tex.getPixelWidth(), h = tex.getPixelHeight();
    final int x2 = x+w, y2 = y+h;
    tex.bind();
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex2i(x, y2);
    glTexCoord2f(1.0f, 0.0f); glVertex2i(x2, y2);
    glTexCoord2f(1.0f, 1.0f); glVertex2i(x2, y);
    glTexCoord2f(0.0f, 1.0f); glVertex2i(x, y);
    glEnd();
  }
}
