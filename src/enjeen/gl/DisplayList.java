package enjeen.gl;

import static org.lwjgl.opengl.GL11.*;

public class DisplayList {
  final int display_list;
  boolean recorded;

  public DisplayList() {
    display_list = glGenLists(1);
    recorded = false;
  }

  public void record() {
    glNewList(display_list, GL_COMPILE);
  }

  public void finishRecording() {
    glEndList();
    recorded = true;
  }

  public void call() {
    if (!recorded) {
      throw new enjeen.EnjeenError("DisplayList called but not recorded yet");
    }
    glCallList(display_list);
  }
}
