package enjeen.gl;

import enjeen.EnjeenError;
import enjeen.Log;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import org.lwjgl.opengl.GL11;

/**
 * A TrueType font implementation originally for Slick, then edited for enjeen by Max.
 *
 * original author James Chambers (Jimmy)
 * original author Jeremy Adams (elias4444)
 * original author Kevin Glass (kevglass)
 * original author Peter Korzuszek (genail)
 * then edited by David Aaron Muhar (bobjob)
 * then edited by Maksim Sipos (max) to cleanup some messy code and fix texture bug.
 */
public class TrueTypeFont {
  public final static int ALIGN_LEFT = 0, ALIGN_RIGHT = 1, ALIGN_CENTER = 2;

  /** Boolean flag on whether AntiAliasing is enabled or not */
  private boolean antiAlias = false;
  /** Font's size */
  private int fontSize = 0;
  /** Font's height */
  private int fontHeight = 0;
  public int getHeight() { return fontHeight; }

  /** Default font texture width & height. */
  private int textureWidth = 256, textureHeight = 256;
  private final int correctL = 9, correctR = 8;

  private Texture texture;
  public Texture getTexture() { return texture; }

  /** Map that holds necessary information about the font characters */
  private final HashMap<Character, CharacterLocation> charLocations;
  private class CharacterLocation {
    /** Character's width */
    public int width;
    /** Character's height */
    public int height;
    /** Character's stored x position */
    public int storedX;
    /** Character's stored y position */
    public int storedY;
  }


  private TrueTypeFont(final float size) {
    charLocations = new HashMap<Character, CharacterLocation>();
    if (size > 24.0f) {
      textureWidth = 512; textureHeight = 512;
    }
  }


  public static TrueTypeFont newFromFile(final File file, final float size) {
    try {
      final Font awtFont = Font.createFont(Font.TRUETYPE_FONT, file).deriveFont(size);
      return newFromFont(awtFont);
    } catch (FontFormatException e) {
      throw new EnjeenError(String.format("File '%s' not a true type font", file.getAbsolutePath()), e);
    } catch (IOException e) {
      throw new EnjeenError(String.format("IO error loading '%s'", file.getAbsolutePath()), e);
    }
  }


  public static TrueTypeFont newFromStream(final InputStream is, final float size) {
    try {
      final Font awtFont = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(size);
      return newFromFont(awtFont);
    } catch (FontFormatException e) {
      throw new EnjeenError("from input stream: not a true type font", e);
    } catch (IOException e) {
      throw new EnjeenError("IO error while loading TTF from input stream", e);
    }
  }


  public static TrueTypeFont newFromFont(final Font awtFont) {
    final TrueTypeFont ttf = new TrueTypeFont(awtFont.getSize());
    ttf.fontSize = awtFont.getSize() + 3;
    ttf.createSet(awtFont);
    return ttf;
  }


  private BufferedImage getFontImage(Font font, char ch) {
    // Create a temporary image to extract the character's size
    BufferedImage tempfontImage = new BufferedImage(1, 1,
                                                    BufferedImage.TYPE_INT_ARGB);
    Graphics2D g = (Graphics2D) tempfontImage.getGraphics();
    if (antiAlias == true) {
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                         RenderingHints.VALUE_ANTIALIAS_ON);
    }
    g.setFont(font);
    final FontMetrics fontMetrics = g.getFontMetrics();
    int charwidth = fontMetrics.charWidth(ch) + 8;

    if (charwidth <= 0) {
      charwidth = 7;
    }
    int charheight = fontMetrics.getHeight() + 3;
    if (charheight <= 0) {
      charheight = fontSize;
    }

    // Create another image holding the character we are creating
    BufferedImage fontImage;
    fontImage = new BufferedImage(charwidth, charheight,
                                  BufferedImage.TYPE_INT_ARGB);
    Graphics2D gt = (Graphics2D) fontImage.getGraphics();
    if (antiAlias == true) {
      gt.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                          RenderingHints.VALUE_ANTIALIAS_ON);
    }
    gt.setFont(font);

    gt.setColor(java.awt.Color.WHITE);
    int charx = 3;
    int chary = 1;
    gt.drawString(String.valueOf(ch), (charx), (chary)
      + fontMetrics.getAscent());

    return fontImage;
  }


  private void createSet(Font font) {
    try {
      BufferedImage imgTemp = new BufferedImage(textureWidth, textureHeight, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = (Graphics2D) imgTemp.getGraphics();

      g.setColor(new java.awt.Color(0, 0, 0, 1));
      g.fillRect(0, 0, textureWidth, textureHeight);

      int rowHeight = 0;
      int positionX = 0;
      int positionY = 0;

      String characters = " abcdefghijklmnopqrstuvwxyz" +
                          "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                          "01234567890" +
                          "`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?\n";
      for (int i = 0; i < characters.length(); i++) {
        final char ch = characters.charAt(i);

        final BufferedImage fontImage = getFontImage(font, ch);

        CharacterLocation location = new CharacterLocation();

        location.width = fontImage.getWidth();
        location.height = fontImage.getHeight();

        if (positionX + location.width >= textureWidth) {
          positionX = 0;
          positionY += rowHeight;
          rowHeight = 0;
        }

        location.storedX = positionX;
        location.storedY = positionY;

        if (location.height > fontHeight) {
          fontHeight = location.height;
        }

        if (location.height > rowHeight) {
          rowHeight = location.height;
        }

        // Draw it here
        g.drawImage(fontImage, positionX, positionY, null);

        positionX += location.width;

        charLocations.put(ch, location);
      }

      fontHeight -= 1;
      if (fontHeight <= 0) {
        fontHeight = 1;
      }

      texture = Texture.newFromImage(imgTemp);
      texture.makeModulate();
      texture.makeLinear();
    } catch (Exception e) {
      Log.fatal("Failed to create font.");
    }
  }


  private void drawQuad(float drawX, float drawY, float drawX2, float drawY2,
                        float srcX, float srcY, float srcX2, float srcY2) {
    float DrawWidth = drawX2 - drawX;
    float DrawHeight = drawY2 - drawY;
    float TextureSrcX = srcX / textureWidth;
    float TextureSrcY = srcY / textureHeight;
    float SrcWidth = srcX2 - srcX;
    float SrcHeight = srcY2 - srcY;
    float RenderWidth = (SrcWidth / textureWidth);
    float RenderHeight = (SrcHeight / textureHeight);

    GL11.glTexCoord2f(TextureSrcX, TextureSrcY);
    GL11.glVertex2f(drawX, drawY);
    GL11.glTexCoord2f(TextureSrcX, TextureSrcY + RenderHeight);
    GL11.glVertex2f(drawX, drawY + DrawHeight);
    GL11.glTexCoord2f(TextureSrcX + RenderWidth, TextureSrcY + RenderHeight);
    GL11.glVertex2f(drawX + DrawWidth, drawY + DrawHeight);
    GL11.glTexCoord2f(TextureSrcX + RenderWidth, TextureSrcY);
    GL11.glVertex2f(drawX + DrawWidth, drawY);
  }


  public int getWidth(String text) {
    int totalwidth = 0;
    for (int i = 0; i < text.length(); i++) {
      char currentChar = at(text, i);
      totalwidth += charLocations.get(currentChar).width;
    }
    return totalwidth;
  }


  private char at(String s, int i) {
    char c = s.charAt(i);
    if (charLocations.containsKey(c)) {
      return c;
    } else {
      return '_';
    }
  }


  public void drawString(float x, float y, String text) {
    drawString(x, y, text, 1.0f, 1.0f);
  }


  public void drawString(float x, float y, String text, float scaleX, float scaleY) {
    drawString(x, y, text, 0, text.length() - 1, scaleX, scaleY, ALIGN_LEFT);
  }


  public void drawString(float x, float y, String text, float scaleX, float scaleY, int format) {
    drawString(x, y, text, 0, text.length() - 1, scaleX, scaleY, format);
  }


  public void drawString(float x, float y, String text, int startIndex, int endIndex,
                         float scaleX, float scaleY, int format) {
    assert text != null;

    CharacterLocation charLocation = null;
    char charCurrent;


    int totalwidth = 0;
    int i = startIndex, d, c;
    float startY = 0;



    switch (format) {
      case ALIGN_RIGHT: {
        d = -1;
        c = correctR;

        while (i < endIndex) {
          if (text.charAt(i) == '\n') {
            startY -= fontHeight;
          }
          i++;
        }
        break;
      }
      case ALIGN_CENTER: {
        for (int l = startIndex; l <= endIndex; l++) {
          charCurrent = at(text, l);
          if (charCurrent == '\n') {
            break;
          }
          totalwidth += charLocations.get(charCurrent).width - correctL;
        }
        totalwidth /= -2;
      }
      case ALIGN_LEFT:
      default: {
        d = 1;
        c = correctL;
        break;
      }

    }

    texture.bind();
    GL11.glBegin(GL11.GL_QUADS);

    while (i >= startIndex && i <= endIndex) {

      charCurrent = at(text, i);
      charLocation = charLocations.get(charCurrent);

      if (charLocation != null) {
        if (d < 0) {
          totalwidth += (charLocation.width - c) * d;
        }
        if (charCurrent == '\n') {
          startY -= fontHeight * d;
          totalwidth = 0;
          if (format == ALIGN_CENTER) {
            for (int l = i + 1; l <= endIndex; l++) {
              charCurrent = at(text, l);
              if (charCurrent == '\n') {
                break;
              }
              charLocation = charLocations.get(charCurrent);
              totalwidth += charLocation.width - correctL;
            }
            totalwidth /= -2;
          }
          //if center get next lines total width/2;
        } else {
          drawQuad((totalwidth + charLocation.width) * scaleX + x, startY * scaleY + y,
                   totalwidth * scaleX + x,
                   (startY + charLocation.height) * scaleY + y, charLocation.storedX + charLocation.width,
                   charLocation.storedY + charLocation.height, charLocation.storedX,
                   charLocation.storedY);
          if (d > 0) {
            totalwidth += (charLocation.width - c) * d;
          }
        }
        i += d;

      }
    }
    GL11.glEnd();
  }

  public void destroy() {
    texture.destroy();
  }
}