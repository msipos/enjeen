package enjeen.gl.view;

import enjeen.EnjeenError;
import static org.lwjgl.opengl.GL11.*;

public class ProjectionIdentity extends Projection {
  @Override
  public void set() {
    glMatrixMode(GL_PROJECTION); EnjeenError.checkGL();
    glLoadIdentity(); EnjeenError.checkGL();
  }
}
