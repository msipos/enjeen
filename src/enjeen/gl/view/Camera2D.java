package enjeen.gl.view;

import enjeen.EnjeenError;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.glu.GLU;

public class Camera2D extends Camera {
  private final float width, height;

  public Camera2D(float width, float height) {
    this.width = width; this.height = height;
  }

  @Override
  public void set() {
    glMatrixMode(GL_MODELVIEW); EnjeenError.checkGL();
    glLoadIdentity(); EnjeenError.checkGL();
    GLU.gluOrtho2D(0.0f, width, 0.0f, height); EnjeenError.checkGL();
  }
}
