package enjeen.gl.view;

import enjeen.EnjeenError;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.glu.Project;

public class CameraLookAt extends Camera {
  public float x = 0.0f, y = 0.0f, z = 0.0f;
  public float dx = 0.0f, dy = 0.0f, dz = 0.0f;

  @Override
  public void set() {
    glMatrixMode(GL_MODELVIEW); EnjeenError.checkGL();
    glLoadIdentity(); EnjeenError.checkGL();
    Project.gluLookAt(x, y, z, dx, dy, dz, 0.0f, 0.0f, 1.0f); EnjeenError.checkGL();
  }
}
