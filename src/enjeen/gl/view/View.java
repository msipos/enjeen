package enjeen.gl.view;

public class View {
  private final ViewPort viewPort;
  private final Projection projection;
  private final Camera camera;

  public Camera getCamera() {
    return camera;
  }

  public Projection getProjection() {
    return projection;
  }

  public ViewPort getViewPort() {
    return viewPort;
  }

  public View(ViewPort vp, Projection proj, Camera cam) {
    this.viewPort = vp;
    this.projection = proj;
    this.camera = cam;
  }

  public void set() {
    viewPort.set();
    projection.set();
    camera.set();
  }


  public static View create2D(int width, int height) {
    return new View(new ViewPort(width, height), new ProjectionIdentity(), new Camera2D(width, height));
  }


  public static View create2D(int x, int y, int width, int height) {
    return new View(new ViewPort(x, y, width, height), new ProjectionIdentity(), new Camera2D(width, height));
  }
}
