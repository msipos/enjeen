package enjeen.gl.view;

import enjeen.EnjeenError;
import enjeen.GameWindow;
import org.lwjgl.opengl.GL11;

public class ViewPort {
  private final int x, y, width, height;


  public ViewPort(GameWindow window) {
    this(0, 0, window.getWidth(), window.getHeight());
  }


  public ViewPort(int width, int height) {
    this(0, 0, width, height);
  }


  public ViewPort(int x, int y, int width, int height) {
    this.x = x; this.y = y; this.width = width; this.height = height;
  }


  public void set() {
    GL11.glViewport(x, y, width, height); EnjeenError.checkGL();
  }
}
