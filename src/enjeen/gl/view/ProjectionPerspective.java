package enjeen.gl.view;

import enjeen.EnjeenError;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.glu.Project;

public class ProjectionPerspective extends Projection {
  private final float fovy;
  private final float aspect;
  private final float zNear;
  private final float zFar;

  public ProjectionPerspective(float fovy, float aspect, float zNear, float zFar) {
    this.fovy = fovy;
    this.aspect = aspect;
    this.zNear = zNear;
    this.zFar = zFar;
  }

  @Override
  public void set() {
    glMatrixMode(GL_PROJECTION); EnjeenError.checkGL();
    glLoadIdentity(); EnjeenError.checkGL();
    Project.gluPerspective(fovy, aspect, zNear, zFar); EnjeenError.checkGL();
  }
}
