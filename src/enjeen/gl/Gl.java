package enjeen.gl;

import static org.lwjgl.opengl.GL11.*;

/** General enabling of GL features. Make sure these are all called after Display has been created. */
public class Gl {
  public static void enableAlphaBlend() {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }
}
