package enjeen.event;

import enjeen.EnjeenError;
import enjeen.GameWindow;
import enjeen.Log;
import java.util.ArrayList;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class MainLoop {
  private boolean running;
  public boolean isRunning() { return running; }
  public void setRunning(boolean running) { this.running = running; }


  private final ArrayList<UpdateEvent> updateEvents = new ArrayList<UpdateEvent>();
  private final ArrayList<Draw2DEvent> draw2DEvents = new ArrayList<Draw2DEvent>();
  private final ArrayList<RenderEvent> renderEvents = new ArrayList<RenderEvent>();


  private final GameWindow window;


  public MainLoop(GameWindow window) {
    this.window = window;
  }


  public void addHandler(EnjeenEvent handler) {
    if (handler instanceof UpdateEvent) {
      updateEvents.add((UpdateEvent) handler);
    }
    if (handler instanceof Draw2DEvent) {
      draw2DEvents.add((Draw2DEvent) handler);
    }
    if (handler instanceof RenderEvent) {
      renderEvents.add((RenderEvent) handler);
    }
  }


  public final void loop() {
    running = true;

    try {
      if (!Keyboard.isCreated()) Keyboard.create();
      if (!Mouse.isCreated()) Mouse.create();
    } catch (LWJGLException ex) {
      throw new EnjeenError(ex);
    }

    // Main event loop
    long prev_time = System.currentTimeMillis();
    long fps_prev_time = prev_time;
    int fps_frames = 0;


    for (;;) {
      // Check for close event.
      if (Display.isCloseRequested()) {
//        if (!eventClose()) break; if (!running) break;
        break;
      }

      // Calculate times
      final long current_time = System.currentTimeMillis();
      final int delta = (int) (current_time - prev_time);

      // Run update events.
      for (UpdateEvent eventHandler : updateEvents) {
        eventHandler.update(delta);
      }
      if (!running) break;

      // Clear the display
      GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

      // Render 3D stuff:
      for (RenderEvent eventHandler : renderEvents) {
        eventHandler.render();
      }

      // Draw 2D stuff:
      window.getView().set();
      for (Draw2DEvent eventHandler : draw2DEvents) {
        eventHandler.draw2d();
      }

      // Update display
      Display.update();

      // Calculate FPS.
      fps_frames++;
      if (current_time - fps_prev_time > 2000) {
        fps_prev_time = current_time;
        Log.info("FPS = %d", fps_frames / 2);
        fps_frames = 0;
      }

      // Record time
      prev_time = current_time;
    }


    Keyboard.destroy();
    Mouse.destroy();
  }
}
