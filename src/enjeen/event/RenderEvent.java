package enjeen.event;

public interface RenderEvent extends EnjeenEvent {
  public void render();
}
