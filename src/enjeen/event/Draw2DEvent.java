package enjeen.event;

public interface Draw2DEvent extends EnjeenEvent {
  public void draw2d();
}
