package enjeen.event;

public interface UpdateEvent extends EnjeenEvent {
  /** Delta is the time in milliseconds since the last update */
  void update(int delta);
}
