
package enjeen.al;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;

public class Context {
  public static void create() {
    try {
      AL.create();
      checkError();
    } catch (LWJGLException ex) {
      throw new OpenALError(ex);
    }
  }


  public static void checkError() {
    int error = AL10.alGetError();
    String message = "unknown error";

    if (error != AL10.AL_NO_ERROR) {
      switch (error) {
        case AL10.AL_INVALID_NAME: message = "invalid name"; break;
        case AL10.AL_INVALID_ENUM: message = "invalid enum"; break;
        case AL10.AL_INVALID_VALUE: message = "invalid value"; break;
        case AL10.AL_INVALID_OPERATION: message = "invalid operation"; break;
        case AL10.AL_OUT_OF_MEMORY: message = "out of memory"; break;
      }
      throw new OpenALError(message);
    }
  }
}
