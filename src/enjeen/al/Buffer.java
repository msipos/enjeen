package enjeen.al;

import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;

public class Buffer {
  final int bufferId;


  public Buffer () {
    bufferId = AL10.alGenBuffers();
    Context.checkError();
  }


//  public void bufferData(MonoSnippet s) {
//    final int l = s.getLength();
//    ShortBuffer sb = BufferUtils.createShortBuffer(l);
//    for (int i = 0; i < l; i++) {
//      sb.put(i, (short) s.get(i));
//    }
//    AL10.alBufferData(bufferId, AL10.AL_FORMAT_MONO16, sb, 44100);
//    Context.checkError();
//  }
//
//
//  public void bufferData(MonoSnippet left, MonoSnippet right) {
//    final int l = left.getLength();
//    ShortBuffer sb = BufferUtils.createShortBuffer(l*2);
//    for (int i = 0; i < l; i++) {
//      sb.put(i*2, (short) left.get(i));
//      sb.put(i*2+1, (short) right.get(i));
//    }
//    AL10.alBufferData(bufferId, AL10.AL_FORMAT_STEREO16, sb, 44100);
//    Context.checkError();
//  }
}
