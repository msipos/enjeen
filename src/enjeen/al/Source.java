package enjeen.al;

import org.lwjgl.openal.AL10;

public class Source {
  private final int sourceId;

  public Source() {
    sourceId = AL10.alGenSources(); Context.checkError();
  }


  public void setBuffer(Buffer b) {
    AL10.alSourcei(sourceId, AL10.AL_BUFFER, b.bufferId); Context.checkError();
  }


  public void queue(Buffer b) {
    AL10.alSourceQueueBuffers(sourceId, b.bufferId);
  }


  public int unqueue(int millis) {
    while (AL10.alGetSourcei(sourceId, AL10.AL_BUFFERS_PROCESSED) == 0) {
      try { Thread.sleep(millis); } catch (InterruptedException ex) { }
    }

    return AL10.alSourceUnqueueBuffers(sourceId);
  }


  public void setGain(float gain) {
    AL10.alSourcef(sourceId, AL10.AL_GAIN, gain); Context.checkError();
  }


  public void setPitch(float pitch) {
    AL10.alSourcef(sourceId, AL10.AL_PITCH, pitch); Context.checkError();
  }


  public void play() {
    AL10.alSourcePlay(sourceId); Context.checkError();
  }


  public void stop() {
    AL10.alSourceStop(sourceId); Context.checkError();
  }


  public void pause() {
    AL10.alSourcePause(sourceId); Context.checkError();
  }
}
