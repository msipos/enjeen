package enjeen.al;

public class OpenALError extends RuntimeException {
  private static final long serialVersionUID = -65426217878292773L;


  public OpenALError(Throwable cause) { super(cause); }
  public OpenALError(String message) { super(message); }
  public OpenALError() { }
}
