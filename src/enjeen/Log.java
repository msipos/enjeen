package enjeen;

public class Log {
  static public void info(String format, Object... args) {
    System.err.print("INFO: ");
    System.err.printf(format, args);
    System.err.println();
  }

  static public void fatal(String format, Object... args) {
    System.err.print("FATAL:");
    System.err.printf(format, args);
    System.err.println();
    System.exit(1);
  }
}
